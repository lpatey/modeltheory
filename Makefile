.PHONY: all clean

FILE=modeltheory

all:
	pdflatex $(FILE).tex
	bibtex $(FILE)
	pdflatex $(FILE).tex
	pdflatex $(FILE).tex

mrproper: clean

clean:
	rm -Rf *.log *.aux *.toc *.idx *.ilg *.bbl *.ind *.blg *.tdo
